from gi.repository import Gtk
from gi.repository import WebKit
import os.path

class adesk_menu():

	def load(self, content_wallpaper, command):
		self.wallpaper = WebKit.WebView()
		self.command = command
		#self.webviews.set_size_request(400,300)

		self.wallpaper.open("http://localhost:61988/amenu/index.html")

		self.wallpaper.connect("new-window-policy-decision-requested", self.popup)
		self.wallpaper.show()
		#self.wallpaper.Scrollable(false)
		content_wallpaper.attach(self.wallpaper, 0, 0, 1, 1)
		self.wallpaper.set_size_request(200,100)


	def hide(self):
		self.wallpaper.hide()

	def popup(self, view, frame, request, nav_action, policy_decision):

		popup_url = request.get_uri()

		if popup_url.endswith("/facebook"):
			self.command.set_text(popup_url)

		if popup_url.endswith("/google"):
			self.command.set_text(popup_url)

		self.wallpaper.hide()
