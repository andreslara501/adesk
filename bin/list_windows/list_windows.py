#!/usr/bin/env python
# -*- coding: utf-8 -*-

#==================================#
#              Adesk               #
#==================================#
#                                  #
# Application: List Windows        #
# Internal Name: list_windows      #
# Versión: 0.1 - 31/7/2014         #
#                                  #
#==================================#

#Webapp in type_ap

from gi.repository import Gtk
from gi.repository import WebKit
import threading
import os.path
import string
from urllib import unquote


#thread
threads = list()

def unquote_u(source):
	result = unquote(source)
	if '%u' in result:
		result = result.replace('%u','\\u').decode('unicode_escape')
	return result

class list_windows():
	array_windows = []
	array_index = []
	left_id = 1

	def __init__(self, content_windows, command):
		self.content_windows = content_windows
		self.command = command

	def new_window(self, title, application, type_application):
		id = len(self.array_windows)
		self.left_id = id

		window_objs = window_obj(title, application, type_application, id, self.command)

		self.array_windows.append(window_objs.window())
		self.array_index.append(id)

		self.content_windows.attach(self.array_windows[id], 0, 0, 1, 1)
		self.array_windows[id].show()	

	def show_click(self,aa, i):
		count_obj = len(self.array_windows)

		for j in range(0, count_obj):
			self.array_windows[j].hide()

		#Show Window
		self.array_windows[i].show()
		self.array_windows[i].get_child().grab_focus()
		self.array_windows[i].get_child().execute_script('$("#input_menu").focus()')

class window_obj():
	def __init__(self, title, application, type_application, id, command):
		self.command = command
		self.id = id
		self.title = title
		self.application = application
		self.type_application = type_application

	def window(self):
		webkit = WebKit.WebView()
		webkit.open("http://localhost:61988/"+self.application+"/index.html")
		webkit.connect("new-window-policy-decision-requested", self.popup)

		#WebApp online application
		if self.type_application == "webapp":
			webkit.connect("load-finished", self.accessible_code,webkit)
			webkit.connect("load-started", self.accessible_code,webkit)
			webkit.connect("load-progress-changed", self.accessible_code,webkit)

		webkit.set_zoom_level(1)
		webkit.show()

		self.content_webkit = Gtk.ScrolledWindow()
		self.content_webkit.show()
		self.content_webkit.add(webkit)

		return self.content_webkit

	def accessible_code(aa, ee, ii, webkit):
		file_css = open('usr/share/themes/black/webapp.cssa', 'r')
		file_js = open('usr/share/themes/black/webapp.js', 'r')

		aaa = """

			var style = document.createElement('style');
			style.type = 'text/css';
			style.innerHTML = '""" + file_css.read() + """';

			""" + file_js.read() + """

		"""
		webkit.execute_script(aaa)

	def popup(self, view, frame, request, nav_action, policy_decision):
		popup_url = request.get_uri()
		popup_url =  unquote_u(popup_url)

		if popup_url.endswith("/stop"):
			list_windows.array_windows[self.id].destroy()
			list_windows.array_index[self.id] = 0

			active_position = self.id
			position_ok = 0

			while (position_ok != 1):
				if list_windows.array_index[active_position] >= 1:
					position_ok = 1

				else:
					active_position = active_position - 1

			#Show Window
			list_windows.array_windows[active_position].show()

			self.command.set_text("app close " + str(self.id))
			print self.id
		array_url = popup_url.split('/')

		if array_url[4] == "app":
			if array_url[5] == "open":
				self.command.set_text("app open " + array_url[6] + " " + array_url[7] + " " + array_url[8] + " " + array_url[9])
				self.audio(0, 0, "Abriendo " + array_url[9])

		if array_url[4] == "audio":
			self.audio(0, 0, array_url[5])

	def audio(aa, ee, ii, text):
		t = threading.Thread(target=audio_festival, args=(text,))
		threads.append(t)
		t.start()


def audio_festival(text):
	os.system('echo "' + text + '" | iconv -f utf-8 -t iso-8859-1 | festival --tts')
