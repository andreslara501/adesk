#!/usr/bin/env python
# Este archivo usa el encoding: utf-8

try:
	from gi.repository import Gtk
	from gi.repository import Gdk
	import os.path
	import SimpleHTTPServer
	from bin.background import background
	from bin.adesk_menu import adesk_menu
	from bin.list_windows import list_windows
	from gi.repository import WebKit
	import SocketServer
	import threading
	import sys
	import urllib
	from urllib import unquote

except:
	print "**** Adesk: Can't load GTK or Webkit library ****"

def unquote_u(source):
	result = unquote(source)
	if '%u' in result:
		result = result.replace('%u','\\u').decode('unicode_escape')
	return result

class main:
	def __init__(self):

		#Start components
		self.theme()
		self.windows()
		#self.background_load()
		self.taskbar()
		Gtk.main()


	def __call__(self):
		print "ok"


	print '\033[95m' + '** Adesk: Started **' + '\033[0m'

	def close(self,aa,ee):
		try:
			t.stopped = True
			urllib.urlopen('http://localhost:61988/stop')
		except:
			print "bye"

		try:
			t.stopped = True
			urllib.urlopen('http://localhost:61987/stop')
			
		except:
			print "bye"
		sys.exit()


	def theme(self):
		#Including path of system and basic interface
		import os.path

		#Load file interface
		self.builder = Gtk.Builder()
		self.builder.add_from_file("bin/interface/start.glade")		

		#Declare variable content window 
		self.content = self.builder.get_object('content_windows')

		self.command = self.builder.get_object('command')
		self.command.connect("changed", self.command_shell)


		#Library GTK to CSS styles
		style_provider = Gtk.CssProvider()

		#The basic theme is "black"
		try:
			css = open('usr/share/themes/black/basic.css','r')
		except:
			print "**** Adesk: Can't load the CSS file, Adesk load without theme ****"

		try:
			css_data = css.read()
			css.close()
			style_provider.load_from_data(css_data)

			Gtk.StyleContext.add_provider_for_screen(
				Gdk.Screen.get_default(), 
				style_provider,     
				Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
			)
		except Exception, e:
			print "**** Adesk: Sintaxis problem in the CSS file, Adesk load without theme ****"
			print e


	def windows(self):

		self.window = self.builder.get_object('window')
		self.window.fullscreen()

		#When quit
		self.window.connect("delete-event", self.close)

		self.window.show_all()


	def taskbar(self):

		#Calling to taskbar
		self.taskbar = self.builder.get_object('taskbar')
		self.taskbar_buttons = self.builder.get_object('taskbar_buttons')
		self.webkit = WebKit.WebView()
		self.webkit.open("http://localhost:61988/taskbar/index.html")


		self.content_webkit = Gtk.ScrolledWindow()
		self.content_webkit.add(self.webkit)
		self.content_webkit.set_min_content_height(50)
		self.webkit.show()
		self.taskbar.add(self.content_webkit)
		self.content_webkit.show()

		self.webkit.connect("new-window-policy-decision-requested", self.popup)

		self.list_windows = list_windows.list_windows(self.content, self.command)


		#self.list_windows.new_window('Login', 'login', '')
		self.list_windows.new_window('Inicio', 'amenu', '')
		#self.list_windows.new_window('Escritorio', 'background', '')


		self.list_windows.show_click(0,0)

	def popup(self, view, frame, request, nav_action, policy_decision):
		popup_url = request.get_uri()
		popup_url =  unquote_u(popup_url)

		array_url = popup_url.split('/')

		self.command.set_text(array_url[4] + " " + array_url[5] + " " + array_url[6])




	def background_load(self):
		self.background = background.background()
		self.background.load(self.content_wallpaper);

	def command_shell(self, text):
		array_command = self.command.get_text().split(' ')

		if array_command[0] == "app":
			if array_command[1] == "open":
				self.list_windows.new_window(array_command[4], array_command[2], array_command[3])
				self.list_windows.show_click(0, self.list_windows.left_id)

				self.webkit.execute_script("taskbar.add('" + array_command[4] + "', '" + array_command[5] + "', '" + array_command[2] + "');")

			if array_command[1] == "close":
				self.webkit.execute_script("taskbar.close('" + array_command[2] + "');")

		if array_command[0] == "window":
			if array_command[1] == "select":
				self.list_windows.show_click(0, int(array_command[2]))




def appcenter():
	os.system("python appcenter.py")
	return

def files():
	os.system("python files.py")
	return

#thread
threads = list()


t = threading.Thread(target=main)
threads.append(t)
t.start()

t = threading.Thread(target=appcenter)
threads.append(t)
t.start()

t = threading.Thread(target=files)
threads.append(t)
t.start()





