#!/usr/bin/env python
# -*- coding: utf-8 -*-

#==================================#
#              Adesk               #
#==================================#
#                                  #
# Application: Adesk-files         #
# Internal Name: files             #
# Versión: 0.2 - 2/10/2014         #
#                                  #
#==================================#

import os
import sys
import BaseHTTPServer
from urlparse import urlparse
import cgi
import urllib, mimetypes

# Port to transmission of Adesk-Files server
PORT = 61987

#Class to start webserver
class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
	print '\033[95m' + '** Adesk-Files: Ok **' + '\033[0m'

	def do_HEAD (self):
		#converting the url to utf8 string
		path_utf8 = urllib.unquote_plus(self.path[1:]).decode('utf8')

		if os.path.isdir(self.path[1:]):
			print "narmal"

		else:

			file = open(self.path[1:], 'rb')
			url = urllib.pathname2url(self.path[1:])
			type_mime = str(mimetypes.guess_type(url))
			
			type_mime_strip = type_mime.split("'")
			
			self.send_response(200)

			self.send_header('Content-type',self.wfile.write(type_mime_strip[1]))

			self.send_header('Access-Control-Allow-Origin', '*')                
			self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
			self.send_header("Access-Control-Allow-Headers", "X-Requested-With")

			self.end_headers()

			self.wfile.write(file.read())

			file.close()


	def do_GET (self):
		#converting the url to utf8 string
		path_utf8 = urllib.unquote_plus(self.path[1:]).decode('utf8')

		if self.path[1:] == 'stop':
			#If the request are "stop", stop server
			self.stop()

		else:
			#If the file exist
			if os.path.exists(path_utf8):

				#Check if the path are directory or file			
				if os.path.isdir(path_utf8):

					#Html's headers
					self.send_response(200)
					self.send_header('Access-Control-Allow-Origin', '*')
					self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
					self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
					self.send_header('Content-type','application/json')
					self.end_headers()

					#Generating json about files into directory
					self.wfile.write("[")

					path_cleaned = urllib.unquote_plus(self.path[1:]).decode('utf8')
					files_string = ""

					list_dir = os.listdir(path_cleaned)

					list_dir = sorted(list_dir)

					try:
						for idir in list_dir:
							if os.path.isdir(path_cleaned + "/" + idir):
								dir_or_file = "dir"
							else:
								dir_or_file = "file"
							path_cleaned = path_cleaned.strip("/")
							files_string = files_string + """
	{
		\"name\": \"""" + idir + """\", 
		\"type\": \"""" + dir_or_file + """\",
		\"path\": \"""" + path_cleaned + "/" + idir + """\"
	},"""

					except:
						print "**** Adesk-files: Error 404. No such directory ****"

					files_string = files_string.strip(", ")

					self.wfile.write(files_string)
					self.wfile.write("\n]")
				else:
					#Else the url are a file
					#Open file to to url
					file = open(path_utf8, 'rb')

					#Checking file's mime-type
					type_mime = str(mimetypes.guess_type(path_utf8))
					type_mime_strip = type_mime.split("'")
					
					#Html's headers
					self.send_response(200)
					self.send_header('Content-type',type_mime_strip[1])
					self.send_header('Access-Control-Allow-Origin', '*')                
					self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
					self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
					self.end_headers()

					self.wfile.write(file.read())

					file.close()
			else:
				#If the file are not exist
				self.send_response(404)
				self.send_header('Access-Control-Allow-Origin', '*')                
				self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
				self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
				self.send_header('Content-type','text/html')
				self.end_headers()

				self.wfile.write("0")


	def do_POST (self):
		#converting the url to utf8 string
		path_utf8 = urllib.unquote_plus(self.path[1:]).decode('utf8')

		#Creating file or read to write
		try:
			file = open(path_utf8, 'wb')

		except:
			print "**** Adesk-files: Could not create or read file ****"

        #Load fields of form
		form = cgi.FieldStorage(
			fp=self.rfile, 
        	headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
				'CONTENT_TYPE':self.headers['Content-Type'],
				})

		file.write(form['file_content'].value);
		file.close()

		#Html's headers
		self.send_response(200)
		self.send_header('Access-Control-Allow-Origin', '*')                
		self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
		self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
		self.end_headers()

		self.wfile.write('Client: %s\n' % str(self.client_address))
		self.wfile.write('Pfad: %s\n' % self.path)
		self.wfile.write('Formulardaten:\n')

		#Form's fields
		for field in form.keys():
 			self.wfile.write('\t%s=%s\n' % (field, form[field].value))

	def stop(self):
		#Stop server
		print '\033[91m' + '** Adesk-Files: Server stopped, bye! **' + '\033[0m'

		httpd.server_close()
		httpd.stopped = True
		httpd.shutdown

		os._exit(0)

httpd = BaseHTTPServer.HTTPServer(('localhost', PORT), RequestHandler)
httpd.serve_forever()

