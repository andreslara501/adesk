/* Declarate */
var audio_control = new audio_control("player", "http://localhost:61987/usr/share/sounds/open_app.ogg", "range_position");

function show_time(){ 
	var date = new Date();
	cad = date.getHours() + ":" + date.getMinutes();
	$("#time").html(cad);
	setTimeout("show_time()",1000); 
}

$(document).ready(function()
{
    $("button").unbind('click');
    $("button").on("click", function()
    {
        $("#taskbar ul li .selected").removeClass('selected');
        $(this).addClass('selected');
    });
    show_time();

    audio_control.play();
});