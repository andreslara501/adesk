//https://github.com/mozilla/pdf.js/wiki/Minimal-example-that-showcases-PDF-rendering-with-text-selection-enabled
//http://www.garysieling.com/blog/building-a-full-text-index-in-javascript
//http://mozilla.github.io/pdf.js/examples/



/* -*- Mode: Java; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */

//
// See README for overview
//

'use strict';

//
// Fetch the PDF document from the URL using promises
//

file = $_GET["file"];

if(file == "undefined")
{
	location.href = "empty.html";
}

var file = $_GET["file"];
var zoom = $_GET["zoom"];
var page_number = parseInt($_GET["page"]);

PDFJS.getDocument(file).then(function(pdf)
{
	/* Using promise to fetch the page */
	pdf.getPage(page_number).then(function(page)
	{
		var scale = zoom;
		var viewport = page.getViewport(scale);

		var maxPages = pdf.pdfInfo.numPages;
		parent.pdf_reader.limit = maxPages;

		/* Prepare canvas using PDF page dimensions */
		var canvas = document.getElementById('the-canvas');
		var context = canvas.getContext('2d');
		canvas.height = viewport.height;
		canvas.width = viewport.width;

		/* Render PDF page into canvas context */
		var renderContext =
		{
			canvasContext: context,
			viewport: viewport
		};
		page.render(renderContext);

		var page = pdf.getPage(page_number);
		var processPageText = function processPageText(pageIndex)
		{
			return function(pageData, content)
			{
				return function(text)
				{
					$('#capturador').append("\n \n Pagina " + page_number + "\n");

					for(i in text.items)
					{
						if(text.items[i]["str"].length > 0)
						{
							$('#capturador').append("\n");
							$('#capturador').append(text.items[i]["str"]);
						}
					}
				}
			}
		}(page_number);

		var processPage = function processPage(pageData)
		{
			var content = pageData.getTextContent();
			content.then(processPageText(pageData, content));
		}
		page.then(processPage);
	});
});