/* Declare */

var db = openDatabase('mydb', '1.0', 'Test DB', 2 * 1024 * 1024);

db.transaction(function (tx) {
	tx.executeSql('CREATE TABLE IF NOT EXISTS LOGS (id unique, log)');
	tx.executeSql('INSERT INTO LOGS (id, log) VALUES (1, "foobar")');
	tx.executeSql('INSERT INTO LOGS (id, log) VALUES (2, "logmsg")');
});

db.transaction(function (tx) {
	tx.executeSql('SELECT * FROM LOGS', [], function (tx, results) {

		var len = results.rows.length, i;
		msg = "<p>Found rows: " + len + "</p>";

		for (i = 0; i < len; i++){
			alert(results.rows.item(i).log );
		}
	}, null);
});

var open_dialog = new open_dialog("aa","aa", "home", "open_dialog_function");
open_dialog.type_file("pdf");

var open_dialog_dir = new open_dialog_dir("aa","aa", "home", "open_dialog_function");

file = $_GET["file"];
//var file = "helloworld.pdf";

var zoom = $_GET["zoom"];
if(typeof(zoom) == "undefined")
{
	var var_timer_configuration_date = setInterval(function(){timer_configuration_date()}, 1000);

	function timer_configuration_date()
	{
		if(configuration_date.state)
		{
			clearInterval(var_timer_configuration_date);
			zoom = configuration_date.object.zoom;
			$("#page").attr("src", "page.html?file=" + file + "&page=" + page_number + "&zoom=" + zoom);
			pdf_reader.zoom = zoom;
		}
	}
}

var page_number = $_GET["page"];
if(typeof(page_number) == "undefined")
{
	page_number = 1.5;
}

var page = new textarea('page', 'page');
var pdf_reader = new pdf_reader(file, zoom);

var back = new button("back");
var next = new button("next");
var zoom_minus = new button("zoom_minus");
var zoom_plus = new button("zoom_plus");

menu.add("Escuchar menú");
menu.add("Abrir");
menu.add("Guardar copia");
menu.add("Escuchar Página");
menu.add("Página actual");
menu.add("Atrás");
menu.add("Siguiente");
menu.add("Buscar");
menu.add("Imprimir");
menu.add("Salir");

/* Object */

function pdf_reader(name_file, zoom)
{
	this.name_file = name_file;
	this.index = 1;
	this.limit = 30;
	this.zoom = zoom;

	this.back = function()
	{
		if(this.index > 1)
		{
			this.index = this.index - 1;
			this.change_page("Cargando página anterior");
		}
		else
		{
			var text = "No hay más páginas atrás";
			audio.speak(encodeURIComponent(text));
		}

		menu.un_select_all();
		menu.return();
	}

	this.next = function()
	{
		if(this.index < (this.limit))
		{
			this.index = this.index + 1;
			this.change_page("Cargando página siguiente");
		}
		else
		{
			var text = "No hay más páginas siguientes";
			audio.speak(encodeURIComponent(text));
		}

		menu.un_select_all();
		menu.return();
	}

	this.change_page = function(text)
	{
		$("#page").attr("src", "page.html?file=" + this.name_file + "&page=" + this.index + "&zoom=" + this.zoom);
		
		this_tmp = this.index;

		$("#actual_page").fadeOut("slow", function()
		{
			$("#actual_page").text("Página " + this_tmp);
		});
		$("#actual_page").fadeIn();

		var text = text + " " + this.index;
		audio.speak(encodeURIComponent(text));
	}

	this.zoom_minus = function()
	{
		if(this.zoom > 0.6)
		{
			this.zoom = this.zoom - 0.2;
		}

		$("#page").attr("src", "page.html?file=" + this.name_file + "&page=" + this.index + "&zoom=" + this.zoom);

		configuration_date.object.zoom = this.zoom;
		configuration_date.rewrite();

		var text = "Menos zoom";
		audio.speak(encodeURIComponent(text));
	}

	this.zoom_plus = function()
	{
		if(this.zoom < 4)
		{
			this.zoom = this.zoom + 0.2;
		}

		$("#page").attr("src", "page.html?file=" + this.name_file + "&page=" + this.index + "&zoom=" + this.zoom);

		configuration_date.object.zoom = this.zoom;
		configuration_date.rewrite();

		var text = "Más zoom";
		audio.speak(encodeURIComponent(text));
	}

	this.update_text = function(text)
	{
		this.actual_text = text;
	}

	this.read_page = function()
	{
		audio.speak(encodeURIComponent(this.actual_text));

		menu.un_select_all();
		menu.return();
	}

	this.actual_page = function()
	{
		var text = "Página actual " + this.index;
		audio.speak(encodeURIComponent(text));

		menu.un_select_all();
		menu.return();
	}
}

/* Functions */

function open_dialog_function()
{
	location.href = "index.html?file=http://localhost:61987/" + open_dialog.path_file;
}

function open_function()
{
	open_dialog.generate();
	open_dialog.show();
}

function save_function()
{
	open_dialog_dir.generate();
	open_dialog_dir.show();
}

function read_page_function()
{
	text = $("#page").contents().find("textarea").val()
	pdf_reader.update_text(text);
	pdf_reader.read_page()
}

function actual_page_function()
{
	pdf_reader.actual_page();
}

function back_function()
{
	pdf_reader.back();
}

function next_function()
{
	pdf_reader.next();
}

function zoom_minus_function()
{
	pdf_reader.zoom_minus();
}

function zoom_plus_function()
{
	pdf_reader.zoom_plus();
}

/* Start */

$(document).ready(function()
{
	/* Menu */

	menu.function_f(2, "open_function");
	menu.function_f(3, "save_function");
	menu.function_f(4, "read_page_function");
	menu.function_f(5, "actual_page_function");
	menu.function_f(6, "back_function");
	menu.function_f(7, "next_function");
	menu.function_f(9, "app.close");

	/* Generates */

	back.function("back_function");
	next.function("next_function");
	zoom_minus.function("zoom_minus_function");
	zoom_plus.function("zoom_plus_function");
	page.generate();

	if(typeof(file) != "undefined")
	{
		$("#file_name").text(file.replace("http://localhost:61987/", ""));
	}
	else
	{
		$("#file_name").text("Ningún documento abierto");
		$("#buttons").hide();
		$("#page").attr("src", "empty.html");
	}
});