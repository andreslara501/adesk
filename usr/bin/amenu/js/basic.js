function amenu()
{
    $.get("http://localhost:61987/usr/bin", function(apps)
    {
        $.each(apps, function(index, app)
        {
            if((app.name != "styles") && (app.name != "libs") && (app.name != "background") && (app.name != "amenu"))
            {
                $.get("http://localhost:61988/" + app.name + "/configuration.json", function(configuration_app)
                {
                    $("#results ul").append("<li onmouseenter=\"audio.speak(encodeURIComponent('" + configuration_app.title_speak + "'));\"><a href=\"app/open/" + app.name + "/" + configuration_app.type + "/" + configuration_app.title + "/" + configuration_app.title_speak + "\" text=\"" + configuration_app.title_speak + "\" target=\"_blank\" text=\"" + configuration_app.title_speak + "\" class=\"application_item speak\"><h3>" + configuration_app.title + "</h3><img src=\"../" + app.name + "/img/icon-black.svg\"></img></a></li>");
                });
            }
        });
    });
}

amenu.prototype.search = function(button)
{
    $("#search").keyup(function(e)
    {
        var search = $(this).val();
        $(".application_item").show();
        
        if(search)
        {
            $(".application_item").not(":contains(" + search + ")").hide();
        }

        if(e.keyCode == 13)
        {
            text = "Lista de programas: ";
            $("#results ul li a:visible").each(function(index, elem)
            {
                text = text + ", " + $(this).attr("text");
            });

            audio.speak(encodeURIComponent(text));
        }
    });
}


function programas_function()
{
    programas.speak();
    programas.select();
};

function busqueda_function()
{
    $("#busqueda input").focus();
};




/*----------------------------------*/
/*              Declare             */

var busqueda = new input('busqueda', 'busqueda');
var programas = new list_simple('programas', 'programas');
var amenu = new amenu();

menu.add("Escuchar menú");
menu.add("Búsqueda");
menu.add("Programas");
menu.add("Configuración");
menu.add("Salir");

$(document).ready(function()
{   
    /*If the html are loaded, ejecute this */
    amenu.search();

    menu.function_f(2, "busqueda_function");
    menu.function_f(3, "programas_function");

    programas.generate();
    busqueda.generate();
});

/*----------------------------------*/