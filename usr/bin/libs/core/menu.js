function menu()
{
    this.buttons = [];
    this.title;
    this.function_r_to_menu;
    this.f_return_to_menutmpFunc;

	this.add = function(button)
	{
	    menu.buttons.push(button);
	}

	this.generate = function()
	{
		$("#menu").prepend('<h2></h2>');
		$("#menu").append('<ul></ul>');
		$("#menu").append('<input type="text" id="input_menu" class="input_system">');

	    $.each(this.buttons, function(index, elem)
	    {
	    	f_index = index + 1;
	        $("#menu ul").append("<li id=\"option_" + f_index + "\"><a href=\"audio/" + elem + "\" text=\"F" + f_index + ", " + elem + "\" target=\"_blank\" >F" + f_index + " | " + elem + "</a></li>");
	    });

	    $("#menu ul li a").mouseover(function(e)
		{
			audio.speak(encodeURIComponent($(this).attr("text")));
		});

		$("#input_menu").on('keydown', null, 'f1', function()
		{
			menu.speak();
		});	

		$("#input_menu").focus();
	}

	this.function_f = function(number, function_add)
	{
		var object = this;

		var codeToRun = function_add + "();";
		var tmpFunc = new Function(codeToRun);
	
		$("#menu ul #option_" + number).click(function(e)
		{		
			object.select(number);
			e.preventDefault();
			tmpFunc();
		});

		$("#input_menu").on('keydown', null, 'f' + number, function()
		{
			object.select(number);
			tmpFunc();
		});
	}

	this.speak = function(button)
	{
		text = "Menú de " + configuration.object.title_speak;
		$("#menu ul li a").each(function(index, elem)
		{
			text = text + ". " + $(this).attr("text");
		});

		audio.speak(encodeURIComponent(text));
	}

	this.title = function(title)
	{
		this.title = title
	}

	this.select = function(number)
	{
		$("#menu ul #option_" + number + " a ").addClass("selected");
	}

	this.unselect = function(number)
	{
		$("#menu ul #option_" + number + " a ").removeClass("selected");
	}

	this.un_select_all = function()
	{
		$("#menu ul li a ").removeClass("selected");
	}

	this.return_to_menu = function(number)
	{

	}

	this.return = function(number)
	{
		$("#input_menu").focus();
	}

	this.function_return_to_menu = function(function_add)
	{
		this.f_return_to_menutmpFunc = function_add + "();";
	}

	this.return_to_menu = function(number)
	{
		$("#input_menu").focus();

		text = "Volviendo al menú inicio";

		audio.speak(encodeURIComponent(text));

		var tmpFunc = new Function(this.f_return_to_menutmpFunc); ;
		tmpFunc();
	}
}

/*Object created*/
var menu = new menu();

$(document).ready(function()
{   
    /*If the html are loaded, ejecute this */
    menu.title(configuration.object.title);
    menu.generate();
});