function taskbar()
{
	this.buttons = [];
	this.id = 0;

	this.add = function(title, title_speak, name_app)
	{
		menu.buttons.push(button);
		$("#taskbar ul li a .selected").removeClass('selected');
		$('#taskbar ul').append("<li><a id='" + this.id + "' href='window/select/" + this.id + "' text='" + title_speak + "' target='_blank'><button class=\"selected\">" + title + "</button></a></li>");

		$("#taskbar ul li a").mouseover(function(e)
		{
			audio.speak(encodeURIComponent($(this).attr("text")));
		});

		$("#taskbar ul li a button").on("click", function()
		{
			$("#taskbar ul li a .selected").removeClass('selected');
			$(this).addClass('selected');
		})

		this.id = this.id + 1;
	}

	this.close = function(id_close)
	{
		$('#taskbar ul li #' + id_close).animate({
			width: 'toggle'
		}, "slow");

		audio_control.change_url("http://localhost:61987/usr/share/sounds/close_app.wav");
		audio_control.play();
	}

	this.speak = function(button)
	{
		text = "Menú de " + configuration.title_speak;
		$("#menu ul li a").each(function(index, elem)
		{
			text = text + ". " + $(this).attr("text");
		});

		audio.speak(encodeURIComponent(text));
	}

	this.select = function(id_select)
	{
		$("#taskbar ul li #" + id_select).addClass("selected");
	}

	this.unselect = function(number)
	{
		$("#menu ul #option_" + number + " a ").removeClass("selected");
	}
}

/*Object created*/
var taskbar = new taskbar();

$(document).ready(function()
{   
	/*If the html are loaded, ejecute this */
	taskbar.add('Inicio', 'Inicio', 'amenu');
});