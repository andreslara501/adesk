/* List and select boxes */

function list_simple(id, name)
{
	this.id = id;
	this.name = name;
	this.index = 1;

	this.back = function()
	{
		if(this.index > 0)
		{
			this.index = this.index - 1;
		}
	}

	this.next = function()
	{
		list_length = $("#" + this.id + " #results ul li").length;

		if(this.index < (list_length - 1))
		{
			this.index = this.index + 1;
		}
	}

	this.generate = function()
	{
		var object = this;

		$("#" + this.id).append('<input type="text" id="input_' + this.id + '" class="input_system">');	

		$("#" + this.id + " input").on('keydown', null, 'up', function()
		{
			object.back();
			$("#" + object.id + " #results ul li a").removeClass('selected');
			$("#" + object.id + " #results ul li a:eq(" + object.index + ")").addClass('selected');

			text = $("#" + object.id + " #results ul li a:eq(" + object.index + ")").attr("text");

			audio.speak(encodeURIComponent(text));
		});   

		$("#" + this.id + " input").on('keydown', null, 'down', function()
		{
			object.next();
			$("#" + object.id + " #results ul li a").removeClass('selected');
			$("#" + object.id + " #results ul li a:eq(" + object.index + ")").addClass('selected');

			text = $("#" + object.id + " #results ul li a:eq(" + object.index + ")").attr("text");

			audio.speak(encodeURIComponent(text));
		});

		$("#" + this.id + " input").keyup(function(e)
		{
			if(e.keyCode == 13)
			{
				jQuery("#" + object.id + " #results ul li a:eq(" + object.index + ")")[0].click();
			}
		});

		$("#" + this.id + " input").on('keydown', null, 'esc', function()
		{
			menu.return_to_menu();
		});
	}

	this.speak = function()
	{
		var text = "Lista con navegación de " + $("#" + this.id + " h2").text();
		audio.speak(encodeURIComponent(text));
	}

	this.select = function()
	{
		$("#" + this.id).focus();
	}
}

function select_multiple(id, name, title, selected, function_add)
{
	this.id = id;
	this.name = name;
	this.title = title;
	this.selected = selected;
	this.options = [];
	this.index = selected;

	this.back = function()
	{
		if(this.index > 0)
		{
			this.index = this.index - 1;
		}

		this.choice(this.index);
	}

	this.next = function()
	{

		list_length = $("#" + id).parent().parent().children("li:not([code_add=syst])").size();

		if(this.index < (list_length))
		{
			this.index = this.index + 1;
		}
		this.choice(this.index);
	}

	this.total_elements = function()
	{
		return this.options.length;
	}

	this.index_update = function(index_new)
	{
		this.index = index_new;
	}

	this.add = function(option)
	{
		this.options.push(option);
	}

	this.generate = function()
	{
		var this_tmp = this;
		var id_selectul = $("#" + this.id).attr("id");
		var name_selectul = $("#" + this.id).attr("name");

		if($("#" + this.id).length)
		{
			var rep = $("#" + this.id)
				.clone()
				.wrap("<div></div>")
				.parent().html()
				.replace(/select/g,"ul type_control='html_select'")
				.replace(/option/g,"li");

			$("#" + this.id).replaceWith(rep);

			$.each(this.options, function (ind, elem)
			{ 
				$("#" + this_tmp.id).append("<li value=\"" + elem[0] + "\">" + elem[1] + "</li>");
			});

			$("#" + this.id).append("<li code_add=\"syst\"><input type=\"text\" value=\"\" id=\"id_tmp\"></li>");
			$("#" + this.id).append("<input class=\"input_system\" type=\"text\" value=\"aaa\" id=\"input_" + this.id + "\">");

			$("#" + this.id + " li:not([code_add=syst])").click(function()
			{
				var value = $(this).attr("value");

				$(this).parent().children("li").children("#" + this_tmp.id).val(value);

				$("#" + this_tmp.id).parent().parent().children(".selected").removeClass("selected");
				$(this).addClass("selected");

				this_tmp.index_update($(this).index() + 1);

				if(function_add.length > 1)
				{
					codeToRun = function_add + "();";
					var tmpFunc = new Function(codeToRun);

					tmpFunc();
				}

				var text = this_tmp.text_index();
				audio.speak(encodeURIComponent(text));
			});

			/* Key down and up*/

			$("#input_" + this.id).on('keydown', null, 'up', function()
			{
				this_tmp.back();

				this.select();
			});   


			$("#input_" + this.id).on('keydown', null, 'down', function()
			{
				this_tmp.next();

				this.select();
			});

			$("#input_" + this.id).on('keydown', null, 'esc', function()
			{
				menu.return_to_menu();
			});

			$("#" + this.id).children("li").children("#id_tmp").attr("id", id_selectul);
			$("#" + this.id).children("li").children("#id_tmp").attr("name", name_selectul);

			$("#" + this.id).attr("id", "");
			$("#" + this.id).attr("name", "");
		}

		if(this.selected)
		{
			$("#" + id).parent().parent().children("li:nth-child(" + this.selected + ")").click();
		}
	}

	this.speak = function()
	{
		var text = "Lista con navegación de " + this.title;
		audio.speak(encodeURIComponent(text));
	}

	this.choice = function(index)
	{
		this.index_update(index);
		$("#" + id).parent().parent().children("li:nth-child(" + this.index + ")").click();

		scroll = (this.index * 50) - 50;

		$("#" + id).parent().parent().animate(
		{
			scrollTop: scroll
		},
		'slow');
	}

	this.select = function()
	{
		$("#input_" + this.id).focus();
		this.speak();
	}

	this.text_index = function()
	{
		return $("#" + id).parent().parent().children("li:nth-child(" + this.index + ")").text();
	}
}

function select_simple(id, name, selected, function_add)
{
	this.id = id;
	this.name = name;
	this.selected = selected;
	this.options = [];
	this.index = selected;

	this.back = function()
	{
		if(this.index > 0)
		{
			this.index = this.index - 1;
		}

		this.select(this.index);
	}

	this.next = function()
	{

		list_length = $("#" + id).parent().parent().children("li:not([code_add=syst])").size();

		if(this.index < (list_length))
		{
			this.index = this.index + 1;
		}
		this.select(this.index);
	}

	this.index_update = function(index_new)
	{
		this.index = index_new;
	}

	this.add = function(option)
	{
		this.options.push(option);
	}

	this.generate = function()
	{
		var this_tmp = this;
		if($("select").length)
		{
			var rep = $("select")
				.clone()
				.wrap("<div></div>")
				.parent().html()
				.replace(/select/g,"ul type_control='html_select_simple'")
				.replace(/option/g,"li");

			$("select").replaceWith(rep);

			$("[type_control=html_select_simple]").prepend("<li id=\"indicator\" value=\"0\"> Seleccione una opcion</li>");

			$.each(this.options, function (ind, elem)
			{ 
				$("[type_control=html_select_simple]").append("<li value=\"" + elem[0] + "\">" + elem[1] + "</li>");
			});

			$("[type_control=html_select_simple]").append("<li code_add=\"syst\"><input type=\"text\" value=\"aaasfass\" id=\"\"></li>");

			$("[type_control=html_select_simple] li:not(#indicator):not([code_add=syst])").click(function()
			{
				var value = $(this).attr("value");
				$(this).parent().children("li").children("input").val(value);

				$("[type_control=html_select_simple] .selected").removeClass("selected");
				$(this).addClass("selected");

				this_tmp.index_update($(this).index() + 1);

				codeToRun = function_add + "();";
				var tmpFunc = new Function(codeToRun);

				tmpFunc();
			});

			$("[type_control=html_select_simple] li:not([code_add=syst])").click(function()
			{

				if($(this).parent().height() < 200)
				{
					$(this).parent().height(200);
					$("[type_control=html_select_simple] #indicator").hide();
					$("[type_control=html_select_simple] li:not(#indicator):not([code_add=syst])").fadeIn();
					$(this).parent().css("overflow", "auto");
				}
				else
				{
					$(this).parent().height(50);
					$("[type_control=html_select_simple] #indicator").text($(this).text());
					$("[type_control=html_select_simple] #indicator").fadeIn();
					$("[type_control=html_select_simple] li:not(#indicator)").hide();
					$(this).parent().css("overflow", "hidden");
				}
			});

			$("[type_control=html_select_simple]").each(function()
			{
				var id_selectul = $(this).attr("id");
				var name_selectul = $(this).attr("name");

				$(this).attr("id", "");
				$(this).attr("name", "");

				$(this).children("li").children("input").attr("id", id_selectul);
				$(this).children("li").children("input").attr("name", name_selectul);
			});
		}
	}

	this.speak = function()
	{
		audio.speak(encodeURIComponent(text));
	}

	this.select = function()
	{
		$("#" + id).parent().parent().children("li:nth-child(" + this.index + ")").click();
	}

	this.text_index = function()
	{
		return $("#" + id).parent().parent().children("li:nth-child(" + this.index + ")").text();
	}
}


/* Inputs */

function input(id, name)
{
	this.id = id;
	this.name = name;
	this.text = "";

	this.generate = function()
	{
		var object = this;

		$("#" + this.id + " input").on('keydown', null, 'tab', function(e)
		{
			e.preventDefault();

			if(input_caret_position(this.id) == $("#" + object.id + " input").val().length)
			{
				audio.speak(encodeURIComponent(object.text));
			}

			else
			{
				if(input_caret_position(this.id) == 0)
				{
					audio.speak(encodeURIComponent(object.text));
				}
				else
				{
					var found = true;
					var left_position = input_caret_position(this.id);
					var right_position = input_caret_position(this.id);

					while(found)
					{
						left_position --;

						if((object.text[left_position] == " ") || (typeof object.text[left_position] == "undefined"))
						{						
							found = false;
						}
					}

					if(left_position < 0)
					{
						left_position = 0;
					}

					found = true;

					while(found)
					{
						right_position ++;

						if((object.text[right_position] == " ") || (typeof object.text[right_position] == "undefined"))
						{						
							found = false;
						}
					}
					
					audio.speak(encodeURIComponent(object.text.slice(left_position, right_position)));
				}
			}
		});

		$("#" + this.id + " input").on('keydown', null, 'left', function(e)
		{	
			
			text = String($("#" + object.id + " input").val());

			text_letters = text[input_caret_position(this.id) - 2] + " " + text[input_caret_position(this.id) -1];
			audio.speak(encodeURIComponent(text_letters));
		});

		$("#" + this.id + " input").on('keydown', null, 'right', function(e)
		{	
			
			text = String($("#" + object.id + " input").val());

			text_letters = text[input_caret_position(this.id)] + " " + text[input_caret_position(this.id) + 1] + ", ";
			audio.speak(encodeURIComponent(text_letters));
		});

		$("#" + this.id + " input").keyup(function(e)
		{
			object.text = $(this).val();

			var letter = String.fromCharCode(e.keyCode);

			if((e.keyCode >= 48) && (e.keyCode <= 126))
			{
				if(vocals.indexOf(letter) > 0)
				{
					letter = "h" + letter;
					audio.speak(encodeURIComponent(letter));
				}
				
				audio.speak(encodeURIComponent(letter));
			}
		});

		$("#" + this.id + " input").on('keydown', null, 'esc', function()
		{
			menu.return_to_menu();

			$("#" + object.id + " input").val("");
		});
	}

	this.speak = function()
	{
		audio.speak(encodeURIComponent(text));
	}

	this.select = function()
	{
		$("#" + this.id).focus();
	}

	function input_caret_position(id)
	{
		ctrl = document.getElementById(id);	

		var CaretPos = 0;	// IE Support
		if(document.selection)
		{
			ctrl.focus ();
			var Sel = document.selection.createRange ();
			Sel.moveStart ('character', -ctrl.value.length);
			CaretPos = Sel.text.length;
		}
		// Firefox support
		else if (ctrl.selectionStart || ctrl.selectionStart == '0')
			CaretPos = ctrl.selectionStart;
		return (CaretPos);
	}
}

function textarea(id, name)
{
	this.id = id;
	this.name = name;
	this.text = "";

	this.generate = function()
	{
		var object = this;

		$("#" + this.id).on('keydown', null, 'tab', function(e)
		{
			e.preventDefault();

			if(input_caret_position(this.id) == $("#" + object.id).val().length)
			{
				audio.speak(encodeURIComponent(object.text));
			}

			else
			{
				if(input_caret_position(this.id) == 0)
				{
					audio.speak(encodeURIComponent(object.text));
				}
				else
				{
					var found = true;
					var left_position = input_caret_position(this.id);
					var right_position = input_caret_position(this.id);

					while(found)
					{
						left_position --;

						if((object.text[left_position] == " ") || (typeof object.text[left_position] == "undefined"))
						{						
							found = false;
						}
					}

					if(left_position < 0)
					{
						left_position = 0;
					}

					found = true;

					while(found)
					{
						right_position ++;

						if((object.text[right_position] == " ") || (typeof object.text[right_position] == "undefined"))
						{						
							found = false;
						}
					}
					
					audio.speak(encodeURIComponent(object.text.slice(left_position, right_position)));
				}
			}
		});

		$("#" + this.id).on('keydown', null, 'left', function(e)
		{	
			
			text = String($("#" + object.id).val());

			text_letters = text[input_caret_position(this.id) - 2] + " " + text[input_caret_position(this.id) -1];
			audio.speak(encodeURIComponent(text_letters));
		});

		$("#" + this.id).on('keydown', null, 'right', function(e)
		{	
			
			text = String($("#" + object.id).val());

			text_letters = text[input_caret_position(this.id)] + " " + text[input_caret_position(this.id) + 1] + ", ";
			audio.speak(encodeURIComponent(text_letters));
		});

		$("#" + this.id).keyup(function(e)
		{
			object.text = $(this).val();

			var letter = String.fromCharCode(e.keyCode);

			if((e.keyCode >= 48) && (e.keyCode <= 126))
			{
				if(vocals.indexOf(letter) > 0)
				{
					letter = "h" + letter;
					audio.speak(encodeURIComponent(letter));
				}
				
				audio.speak(encodeURIComponent(letter));
			}
		});

		$("#" + this.id).on('keydown', null, 'esc', function()
		{
			menu.return_to_menu();

			$("#" + object.id + " input").val("");
		});
	}

	this.speak = function()
	{
		audio.speak(encodeURIComponent(text));
	}

	this.select = function()
	{
		$("#" + this.id).focus();
	}

	function input_caret_position(id)
	{
		ctrl = document.getElementById(id);	

		var CaretPos = 0;	// IE Support
		if(document.selection)
		{
			ctrl.focus ();
			var Sel = document.selection.createRange ();
			Sel.moveStart ('character', -ctrl.value.length);
			CaretPos = Sel.text.length;
		}
		// Firefox support
		else if (ctrl.selectionStart || ctrl.selectionStart == '0')
			CaretPos = ctrl.selectionStart;
		return (CaretPos);
	}
}


/* Buttons */

function button(id)
{
	this.id = id;

	this.select = function()
	{
		$("#" + this.id).addClass("button_selected");
	}

	this.unselect = function()
	{
		$("#" + this.id).removeClass("button_selected");
	}

	this.function = function(function_add)
	{
		var object = this;

		var codeToRun = function_add + "();";
		var tmpFunc = new Function(codeToRun);
	
		$("#" + this.id).click(function(e)
		{		
			e.preventDefault();
			tmpFunc();
		});
	}
}

/* Checkbox */

function checkbox(id)
{
	this.id = id;
	this.checked = true;

	this.generate = function()
	{
		var this_tmp = this;
		$("#" + this.id).parent().addClass("checkbox_label");

		if(this.checked)
		{
			$(this).parent().addClass("checkbox_selected");
		}
		else
		{
			$(this).parent().removeClass("checkbox_selected");
		}

		$("#" + this.id).click(function()
		{
			if($(this).is(':checked'))
			{
				$(this).parent().addClass("checkbox_selected");
			}
			else
			{
				$(this).parent().removeClass("checkbox_selected");
			}
		});
	}

	this.check = function()
	{
		$("#" + this.id).parent().addClass("checkbox_selected");
		$("#" + this.id).click();
	}

	this.uncheck = function()
	{
		$("#" + this.id).parent().removeClass("checkbox_selected");
		$("#" + this.id).click();
	}

	this.checked = function()
	{
		return $("#" + this.id).is(':checked');
	}

	this.function = function(function_add)
	{
		var object = this;

		var codeToRun = function_add + "();";
		var tmpFunc = new Function(codeToRun);
	
		$("#" + this.id).click(function(e)
		{		
			tmpFunc();
		});
	}
}

/* Files */

function open_dialog(id, function_add, path, function_add)
{
	this.id = id;
	this.path = path;
	this.path_file = "";
	this.function_add = function_add;
	this.directory = new select_multiple("directory_" + this.id, "directory_" + this.id, "directory_" + this.id, 0, "");
	var this_tmp = this;
	this.options = [];
	this.type_files = [];
	this_tmp.counter_click = 0;

	this.generate = function()
	{
		var html = "			<div id=\"open_dialog\">" + 
				"<div>" + 
					"<h2>Abrir</h2>" +
					"<label>" +
						"<select name=\"directory_" + this.id + "\" size=\"5\" id=\"directory_" + this.id + "\"></select>" +
					"</label>" +
					"<button id=\"close_dialog\"><span class=\"icon\">&#xf115;</span> Cancelar</button>" +
				"</div>" +
			"</div>";

		$("#app").append(html);

		$("#close_dialog").click(function()
		{
			$("#open_dialog").remove();
			this_tmp.directory.options = [];
		});

		$.get("http://localhost:61987/" + this_tmp.path)
		.done(function(elements)
		{
			$("#open_dialog div h2").text("home/");

			if(this_tmp.path != "home")
			{
				element_directory = ["..", ".. Atrás"];
				this_tmp.directory.add(element_directory);
			}

			$.each(elements, function(index, element)
			{
				if(element.type == "dir")
				{
					dir_or_file = "Carpeta";

					element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
					this_tmp.directory.add(element_directory);
				}
				else
				{
					dir_or_file = "Archivo";

					var format = element.path.split(".");

					if(this_tmp.type_files.length > 0)
					{	
						$.each(this_tmp.type_files, function(index, element_typefile)
						{
							if(format[1] == element_typefile)
							{
								element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
								this_tmp.directory.add(element_directory);
							}
						});
					}
					else
					{
						element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
						this_tmp.directory.add(element_directory);
					}
				}
			});

			this_tmp.directory.generate();
			this_tmp.directory.select();

			$("#input_directory_" + this_tmp.id).on('keydown', null, 'return', function(e)
			{
				//this_tmp.directory.choice(this_tmp.directory.index);

				var file_to_open = $("#open_dialog div label ul .selected").attr("value").split(" , ");

				if($("#open_dialog div label ul .selected").attr("value") == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;
				}
				else
				{
					this_tmp.path = file_to_open[1];
				}

				$("#open_dialog div label ul").remove();

				if(file_to_open[0] == "file")
				{
					this_tmp.path_file = file_to_open[1];

					var codeToRun = this_tmp.function_add + "();";
					var tmpFunc = new Function(codeToRun);
					tmpFunc();

					$("#open_dialog").remove();
				}
				else
				{
					this_tmp.update(this_tmp.path);
				}
			});

			$("#directory_" + this_tmp.id).parent().children().click(function()
			{
				var file_to_open = $(this).val().split(" , ");

				this_tmp.counter_click ++;

				if($(this).val() == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;

					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);

					this_tmp.counter_click = 0;
				}
				else
				{
					this_tmp.path = file_to_open[1];
				}

				if(file_to_open[0] == "file")
				{
					this_tmp.path_file = file_to_open[1];

					var codeToRun = this_tmp.function_add + "();";
					var tmpFunc = new Function(codeToRun);
					tmpFunc();

					$("#open_dialog").remove();
				}
				else
				{
					if(this_tmp.counter_click == 2)
					{
						this_tmp.path = file_to_open[1];
						this_tmp.counter_click = 0;

						$("#open_dialog div label ul").remove();
						this_tmp.update(this_tmp.path);
					}
				}
			});
		});
	}

	this.update = function(new_path)
	{
		this_tmp.directory.options = [];

		var html = "<select name=\"directory_" + this.id + "\" size=\"5\" id=\"directory_" + this.id + "\"></select>";

		$("#open_dialog div h2").text(new_path);

		$("#app #open_dialog div label").append(html);

		$.get("http://localhost:61987/" + new_path)
		.done(function(elements)
		{
			if(this_tmp.path != "home")
			{
				element_directory = ["..", ".. Atrás"];
				this_tmp.directory.add(element_directory);
			}

			$.each(elements, function(index, element)
			{
				if(element.type == "dir")
				{
					dir_or_file = "Carpeta";

					element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
					this_tmp.directory.add(element_directory);
				}
				else
				{
					dir_or_file = "Archivo";

					var format = element.path.split(".");

					if(this_tmp.type_files.length > 0)
					{	
						$.each(this_tmp.type_files, function(index, element_typefile)
						{
							if(format[1] == element_typefile)
							{
								element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
								this_tmp.directory.add(element_directory);
							}
						});
					}
					else
					{
						element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
						this_tmp.directory.add(element_directory);
					}
				}
			});

			this_tmp.directory.generate();
			this_tmp.directory.select();
			this_tmp.directory.index = 0;

			$("#input_directory_" + this_tmp.id).on('keydown', null, 'return', function(e)
			{
				//this_tmp.directory.choice(this_tmp.directory.index);

				var file_to_open = $("#open_dialog div label ul .selected").attr("value").split(" , ");

				if($("#open_dialog div label ul .selected").attr("value") == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;
				}
				else
				{
					this_tmp.path = file_to_open[1];
				}

				$("#open_dialog div label ul").remove();

				if(file_to_open[0] == "file")
				{
					this_tmp.path_file = file_to_open[1];

					var codeToRun = this_tmp.function_add + "();";
					var tmpFunc = new Function(codeToRun);
					tmpFunc();

					$("#open_dialog").remove();
				}
				else
				{
					this_tmp.update(this_tmp.path);
				}
			});

			$("#directory_" + this_tmp.id).parent().children().click(function()
			{
				var file_to_open = $(this).val().split(" , ");

				this_tmp.counter_click ++;

				if($(this).val() == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;


					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);

					this_tmp.counter_click = 0;
				}
				else
				{
					this_tmp.path = file_to_open[1];
				}

				if(file_to_open[0] == "file")
				{
					this_tmp.path_file = file_to_open[1];

					var codeToRun = this_tmp.function_add + "();";
					var tmpFunc = new Function(codeToRun);
					tmpFunc();

					$("#open_dialog").remove();
				}
				else
				{
					if(this_tmp.counter_click == 2)
					{
						this_tmp.path = file_to_open[1];
						this_tmp.counter_click = 0;

						$("#open_dialog div label ul").remove();
						this_tmp.update(this_tmp.path);
					}
				}
			});
		});
	}

	this.show = function(option)
	{
		$("#open_dialog").fadeIn();
	}

	this.type_file = function(type)
	{
		this.type_files.push(type);
	}

	this.add = function(option)
	{
		this.options.push(option);
	}
}

function open_dialog_dir(id, function_add, path, function_add)
{
	this.id = id;
	this.path = path;
	this.path_file = "";
	this.function_add = function_add;
	this.directory = new select_multiple("directory_" + this.id, "directory_" + this.id, "directory_" + this.id, 0, "");
	var this_tmp = this;
	this.options = [];
	this.type_files = [];
	this.counter_click = 0;

	this.generate = function()
	{
		var html = "			<div id=\"open_dialog\">" + 
				"<div>" + 
					"<h2>Abrir</h2>" +
					"<label>" +
						"<select name=\"directory_" + this.id + "\" size=\"5\" id=\"directory_" + this.id + "\"></select>" +
					"</label>" +
					"<input type=\"text\"></input>" +
					"<button id=\"close_dialog\"><span class=\"icon\">&#xf115;</span> Cancelar</button>" +
				"</div>" +
			"</div>";

		$("#app").append(html);

		$("#close_dialog").click(function()
		{
			$("#open_dialog").remove();
			this_tmp.directory.options = [];
		});

		$.get("http://localhost:61987/" + this_tmp.path)
		.done(function(elements)
		{
			$("#open_dialog div h2").text("home/");

			if(this_tmp.path != "home")
			{
				element_directory = ["..", ".. Atrás"];
				this_tmp.directory.add(element_directory);
			}

			$.each(elements, function(index, element)
			{
				if(element.type == "dir")
				{
					dir_or_file = "Carpeta";

					element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
					this_tmp.directory.add(element_directory);
				}
			});

			this_tmp.directory.generate();
			this_tmp.directory.select();

			$("#input_directory_" + this_tmp.id).on('keydown', null, 'return', function(e)
			{
				var file_to_open = $("#open_dialog div label ul .selected").attr("value").split(" , ");

				this_tmp.counter_click ++;

				if($("#open_dialog div label ul .selected").attr("value") == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;

					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);

					this_tmp.counter_click = 0;
				}
				else
				{
					this_tmp.path = file_to_open[1];
					this_tmp.counter_click = 0;

					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);
				}
			});

			$("#directory_" + this_tmp.id).parent().children().click(function()
			{
				var file_to_open = $("#open_dialog div label ul .selected").attr("value").split(" , ");

				this_tmp.counter_click ++;

				if($("#open_dialog div label ul .selected").attr("value") == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;

					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);

					this_tmp.counter_click = 0;
				}
				else
				{
					if(this_tmp.counter_click == 2)
					{
						this_tmp.path = file_to_open[1];
						this_tmp.counter_click = 0;

						$("#open_dialog div label ul").remove();
						this_tmp.update(this_tmp.path);
					}
				}
			});
		});
	}

	this.update = function(new_path)
	{
		this_tmp.directory.options = [];

		var html = "<select name=\"directory_" + this.id + "\" size=\"5\" id=\"directory_" + this.id + "\"></select>";

		$("#open_dialog div h2").text(new_path);

		$("#app #open_dialog div label").append(html);

		$.get("http://localhost:61987/" + new_path)
		.done(function(elements)
		{
			if(this_tmp.path != "home")
			{
				element_directory = ["..", ".. Atrás"];
				this_tmp.directory.add(element_directory);
			}

			$.each(elements, function(index, element)
			{
				if(element.type == "dir")
				{
					dir_or_file = "Carpeta";

					element_directory = [element.type + " , " + element.path, dir_or_file + " " + element.name];
					this_tmp.directory.add(element_directory);
				}
			});

			this_tmp.directory.generate();
			this_tmp.directory.select();
			this_tmp.directory.index = 0;

			$("#input_directory_" + this_tmp.id).on('keydown', null, 'return', function(e)
			{
				var file_to_open = $("#open_dialog div label ul .selected").attr("value").split(" , ");

				this_tmp.counter_click ++;

				if($("#open_dialog div label ul .selected").attr("value") == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;

					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);

					this_tmp.counter_click = 0;
				}
				else
				{
					this_tmp.path = file_to_open[1];
					this_tmp.counter_click = 0;

					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);
				}
			});

			$("#directory_" + this_tmp.id).parent().children().click(function()
			{
				var file_to_open = $("#open_dialog div label ul .selected").attr("value").split(" , ");

				this_tmp.counter_click ++;

				if($("#open_dialog div label ul .selected").attr("value") == "..")
				{
					var posicion = this_tmp.path.lastIndexOf('/');
					var path_back = this_tmp.path.substring(0, posicion);

					this_tmp.path = path_back;

					$("#open_dialog div label ul").remove();
					this_tmp.update(this_tmp.path);

					this_tmp.counter_click = 0;
				}
				else
				{
					if(this_tmp.counter_click == 2)
					{
						this_tmp.path = file_to_open[1];
						this_tmp.counter_click = 0;

						$("#open_dialog div label ul").remove();
						this_tmp.update(this_tmp.path);
					}
				}
			});
		});
	}

	this.show = function(option)
	{
		$("#open_dialog").fadeIn();
	}

	this.type_file = function(type)
	{
		this.type_files.push(type);
	}

	this.add = function(option)
	{
		this.options.push(option);
	}
}

/* Other controls*/

function audio_control(id, file, range_input)
{
	this.id = id;
	this.file = file;

	this.play = function()
	{
		document.getElementById(id).play();
	}

	this.pause = function()
	{
		document.getElementById(id).pause();
	}

	this.volume_left = function()
	{
		document.getElementById(id).volume -= 0.1;
	}

	this.volume_right = function()
	{
		document.getElementById(id).volume += 0.1;
	}

	this.play_pause = function()
	{
		if (this.player.paused)
		{
			this.play();
		}
		else
		{
			this.pause();
		}
	}

	this.change_url = function(url)
	{
		document.getElementById(id).src = url;
	}

	this.generate = function()
	{
		this.player = document.getElementById(id);

		this.player.addEventListener("timeupdate", function()
		{ 
			if(document.getElementById(id).ended)
			{
				playlist.next();
			}

		}, true);
	}
}

/* Start */

$(document).ready(function()
{   

});


