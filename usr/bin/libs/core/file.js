/* Declare */

var files_system = new files_system();

/* Object */

function files_system()
{
	this.state = true;
	this.log = [];

	this.write = function(file_content, url)
	{
		this_tmp = this;
		this_tmp.state = false;

		$.post(url,
		{
			file_content: file_content
		})
		.done(function(data)
		{
			this_tmp.state = true;
			this_tmp.add_log("Writed file: " + url);
		});
	}

	this.add_log = function(text)
	{
		this.log.push(Date.now() + " | " + text);
	}
}