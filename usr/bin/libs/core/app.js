/* Declare */
var var_timer_app = setInterval(function(){its_ok()}, 1000);

function its_ok()
{
	if(configuration.state)
	{
		clearInterval(var_timer_app);
		$("#canvas").fadeOut("3000");
		$("#menu h2").text(configuration.object.title);
	}
}

function app()
{
	this.generate = function()
	{
		var code = "<a href=\"stop\" target=\"_blank\" id=\"button_close\">✕</a>";

		/*var code = "			<div id=\"title_bar\">" +
								"<h1><img id=\"icon\" src=\"img/icon-black.svg\">" + configuration.title + "</h1>" +
								"<a href=\"stop\" target=\"_blank\" id=\"button_close\">✕</a>" +
						"</div>";*/

		$("#app").prepend(code);	

		$("#app").prepend('<div id="canvas"><span><img id=\"icon\" src=\"img/icon-white.svg\"></span></div>');
	}

	this.close = function()
	{
		audio.speak(configuration.title_speak);
		jQuery("#button_close")[0].click();
	}
}

var app = new app();

$(document).ready(function()
{   
		/*If the html are loaded, ejecute this */
		app.generate();

		for(i=0; i<=5; i++)
		{
			$("#canvas span img").fadeTo(1000, 0.13);
			$("#canvas span img").fadeTo(1000, 0.99);
		}

		$("#canvas").delay(2000);

		$("body").css("background-image", "url('http://localhost:61987/usr/share/themes/black/backgroundapp.jpg')");
});
