/* Declare */
var back = new button("back");
var page = new textarea('page1', 'page1');
var open_dialog = new open_dialog("aa","aa", "usr");

menu.add("Escuchar menú");
menu.add("Abrir");
menu.add("Guardar");
menu.add("Nuevo documento");
menu.add("Cambiar nombre");
menu.add("Escuchar documento");
menu.add("Preparar para imprimir");
menu.add("Salir");

/* Functions */

function onclick_function()
{

}

function prueba()
{

}

/* Start */

$(document).ready(function()
{   
	/* Menu */

	menu.function_f(2, "back_function");
	menu.function_f(3, "play_function");
	menu.function_f(4, "pause_function");
	menu.function_f(5, "next_function");
	menu.function_f(6, "playlist_function");
	menu.function_f(9, "app.close");

	back.function("back_function");

	/* Generates */

	open_dialog.generate();

	page.generate();
});