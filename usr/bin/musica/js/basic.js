/* Declare */

configuration.wait = true;

var playlist_selected = false;

var var_timer_wait_configuration = setInterval(function(){timer_wait_configuration()}, 1000);

var exist_playlist;

var audio_control = new audio_control("player", "http://localhost:61987/home/andres/configuration/musica/playlist.json", "range_position");
var playlist = new select_multiple('playlist', 'playlist', "Canciones", 0, "prueba");

/* configurar para que espere*/
//configuration.wait = true;

var back = new button("back");
var play = new button("play");
var pause = new button("pause");
var next = new button("next");

var azar = new checkbox("azar");

menu.add("Escuchar menú");
menu.add("Atrás");
menu.add("Reproducir");
menu.add("Pausar");
menu.add("Siguiente");
menu.add("Lista canciones");
menu.add("Canciones al azar");
menu.add("Salir");

menu.function_return_to_menu("function_return_to_menu");

/* Funtions */

function timer_wait_configuration()
{
	if(configuration.state && configuration_date.state)
	{
		clearInterval(var_timer_wait_configuration);
		if(configuration_date.object.random)
		{
			azar.check();
			menu.select(7);
		}

		playlist.choice(configuration_date.object.last);
	}
}

function state_exist_playlist(respons)
{
	exist_playlist = respons;
}

function back_function()
{
	playlist.back();

	menu.unselect(2);
	menu.select(3);
	menu.return();
};

function play_function()
{
	audio_control.play_pause();
	play.select();
	pause.unselect();
	menu.unselect(4);
	menu.select(3);
	menu.return();
};

function pause_function()
{
	audio_control.pause();
	pause.select();
	play.unselect();
	menu.unselect(3);
	menu.select(4);
	menu.return();
};

function next_function()
{
	if($("#azar").is(':checked'))
	{
		random = Math.floor((Math.random() * playlist.total_elements()) + 1);
		while(random == playlist.index)
		{
			random = Math.floor((Math.random() * playlist.total_elements()) + 1);
		}

		playlist.choice(random);
	}
	else
	{
		playlist.next();
	}

	menu.unselect(5);
	menu.select(3);

	play.select();
	pause.unselect();
	menu.return();
};

function playlist_function()
{
	playlist.select();
	playlist_selected = true;
};

function random_function()
{
	if(azar.checked())
	{
		configuration_date.object.random = 1;
		configuration_date.rewrite();
		azar.check();
		menu.unselect(7);
	}
	else
	{
		configuration_date.object.random = 0;
		configuration_date.rewrite();
		azar.uncheck();
		menu.select(7);
	}

	menu.return();
};

function function_return_to_menu()
{alert("fdjkask");
	menu.unselect(6);
};

function prueba()
{
	audio_control.change_url($("#playlist").val());
	audio_control.play();

	menu.unselect(4);
	menu.select(3);

	$("#title h3").text(playlist.text_index());

	$.get($("#playlist").val(), function(data)
	{
		var view = new jDataView(data);
		aaa = view.getString(3, view.byteLength - 128)
		
		if(aaa == 'TAG')
		{
			var title = view.getString(30, view.tell());
			var artist = view.getString(30, view.tell());
			var album = view.getString(30, view.tell());
			var year = view.getString(4, view.tell());
		}

		$("#cover img").animate(
		{
			opacity: 0.25,
			duration: 50
		}
		,
		{
		   	complete: function()
		   	{
				$.get("http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=5ef5ab5568ded5352c55aca46707307c&format=json&album=" + encodeURIComponent(album) + "&artist=" + encodeURIComponent(artist), function(respons)
				{
					if(respons.album.image[2]["#text"] != undefined)
					{
						$("#cover img").attr("src", respons.album.image[2]["#text"]);
						$("#cover img").load(function()
						{
							$("#cover img").animate(
							{
								opacity: 1,
								duration: 50
							});
						});
					}
				});
			}
		});
	});

	if(!playlist_selected)
	{
		menu.return();
	}

	configuration_date.object.last = playlist.index;
	configuration_date.rewrite();
}

/* Start */

$(document).ready(function()
{   
	/* Menu */

	menu.function_f(2, "back_function");
	menu.function_f(3, "play_function");
	menu.function_f(4, "pause_function");
	menu.function_f(5, "next_function");
	menu.function_f(6, "playlist_function");
	menu.function_f(7, "random_function");
	menu.function_f(9, "app.close");

	back.function("back_function");
	play.function("play_function");
	play.select();
	pause.function("pause_function");
	next.function("next_function");

	azar.function("random_function");

	/* Generates */

	audio_control.generate();
	azar.generate();

	/* Check playlist */

	$.get("http://localhost:61987/home/playlist.txt")
	.done(function(respons)
	{
		var json = JSON.parse(respons);

		$.each(json.files, function(file, name)
		{
			if(title != "" || artist != "")
			{
				name_clear = name.replace(/_/g, " ");
				name_clear = name_clear.replace(".mp3", " ");
				song = ["http://localhost:61987/home/musica/" + name, name_clear];
				playlist.add(song);
			}

			if(playlist.options.length == json.files.length)
			{
				playlist.generate();
			}
		});
		configuration.state = true;
	})
	.fail(function()
	{
		$.get("http://localhost:61987/home/musica/", function(respons)
		{
			state_exist_playlist(respons);
		})
		.done(function(data)
		{
			$.post("http://localhost:61987/home/playlist.txt",
			{
				file_content: exist_playlist 
			})
			.done(function(data)
			{
				$.get("http://localhost:61987/home/playlist.txt").done(function(respons)
				{
					var json = JSON.parse(respons);
					$.each(json.files, function(file, name)
					{
						if(title != "" || artist != "")
						{
							name_clear = name.replace(/_/g, " ");
							name_clear = name_clear.replace(".mp3", " ");
							song = ["http://localhost:61987/home/musica/" + name, name_clear];
							playlist.add(song);
						}

						if(playlist.options.length == json.files.length)
						{
							playlist.generate();
						}
					});
					configuration.state = true;
				});
			});
		});
	});
});