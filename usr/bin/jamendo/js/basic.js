/* Declare */

var playlist_selected = false;

var var_timer_wait_configuration = setInterval(function(){timer_wait_configuration()}, 1000);
setInterval(function(){timer_change_info()}, 5000);

var exist_playlist;
var api_id = "2ce7de63";
var numer_radios = 10;
var actual_radio = 0;
var var_timer_radio = setInterval(function(){timer_load()}, 1000);

/* configurar para que espere*/
configuration.wait = true;

var audio_control = new audio_control("player", "http://localhost:61987/home/andres/configuration/musica/playlist.json", "range_position");
var playlist = new select_multiple('playlist', 'playlist', "Emisoras", 0, "onclick_function");

var back = new button("back");
var play = new button("play");
var pause = new button("pause");
var next = new button("next");

menu.add("Escuchar menú");
menu.add("Atrás");
menu.add("Reproducir");
menu.add("Pausar");
menu.add("Siguiente");
menu.add("Lista emisoras");
menu.add("Salir");

menu.function_return_to_menu("function_return_to_menu");

/* Functions */

function timer_wait_configuration()
{
	if(configuration.state && configuration_date.state)
	{
		clearInterval(var_timer_wait_configuration);

		playlist.choice(configuration_date.object.last);
	}
}

function timer_change_info()
{
	var playlist_val = $("#playlist").val();

	$.get("http://api.jamendo.com/v3.0/radios/stream?client_id=" + api_id + "&id=" + playlist_val + "&format=jsonpretty")
	.done(function(respons)
	{
		$("#title h3").text(respons["results"]["0"]["playingnow"]["artist_name"] + " - " + respons["results"]["0"]["playingnow"]["track_name"] + " | " + respons["results"]["0"]["dispname"]);

		change_albumart(respons["results"]["0"]["playingnow"]["track_image"]);
	});
}

function timer_load() 
{
	if(numer_radios == actual_radio)
	{
		clearInterval(var_timer_radio)
		playlist.generate();
		configuration.state = true;
	}
}

function change_albumart(url)
{
	$("#cover img").attr("src", url);
}

function back_function()
{
	playlist.back();

	menu.unselect(4);
	menu.select(3);
	menu.return();
};

function play_function()
{
	audio_control.play_pause();
	play.select();
	pause.unselect();
	menu.unselect(4);
	menu.select(3);
	menu.return();
};

function pause_function()
{
	audio_control.pause();
	pause.select();
	play.unselect();
	menu.unselect(3);
	menu.select(4);
	menu.return();
};

function next_function()
{
	playlist.next();

	menu.unselect(4);
	menu.select(3);

	play.select();
	pause.unselect();
	menu.return();
};

function playlist_function()
{
	playlist.select();
	playlist_selected = true;
};

function function_return_to_menu()
{
	menu.unselect(6);
};


function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function onclick_function()
{
	var playlist_val = $("#playlist").val();

	$.get("http://api.jamendo.com/v3.0/radios/stream?client_id=" + api_id + "&id=" + playlist_val + "&format=jsonpretty")
	.done(function(respons)
	{
		audio_control.change_url(respons["results"]["0"]["stream"]);
		audio_control.play();
		menu.select(3);

		$("#title h3").text(respons["results"]["0"]["playingnow"]["artist_name"] + " - " + respons["results"]["0"]["playingnow"]["track_name"] + " | " + respons["results"]["0"]["dispname"]);

		$("#cover img").animate(
		{
			opacity: 0.25,
			duration: 50
		}
		,
		{
		   	complete: function()
		   	{
				change_albumart(respons["results"]["0"]["playingnow"]["track_image"]);
				$("#cover img").animate(
				{
					opacity: 1,
					duration: 50
				});
			}
		});
		
		if(!playlist_selected)
		{
			menu.return();
		}
	});

	configuration_date.object.last = playlist.index;
	configuration_date.rewrite();
}

/* Start */

$(document).ready(function()
{   
	/* Menu */

	menu.function_f(2, "back_function");
	menu.function_f(3, "play_function");
	menu.function_f(4, "pause_function");
	menu.function_f(5, "next_function");
	menu.function_f(6, "playlist_function");
	menu.function_f(9, "app.close");

	back.function("back_function");
	play.function("play_function");
	play.select();
	pause.function("pause_function");
	next.function("next_function");

	/* Generates */

	audio_control.generate();

	/* Check playlist */




	for(i=1;i<=10;i++)
	{
		$.get("http://api.jamendo.com/v3.0/radios/stream?client_id=" + api_id + "&id=" + i + "&format=jsonpretty")
		.done(function(respons)
		{
			url = [respons["results"]["0"]["id"], respons["results"]["0"]["dispname"]];
			playlist.add(url);
			actual_radio++;
		});
	}
});