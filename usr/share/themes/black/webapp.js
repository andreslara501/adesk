var generated = 0;


function menu_browser()
{
	this.state = false;
	this.zoom = 1;

	this.generate = function()
	{
		document.getElementsByTagName("head")[0].appendChild(style);

		var h2 = document.createElement('h2');
		h2.className = "adesk_h2";

		var favicon = document.createElement('img');
		favicon.src = "http://www.google.com/s2/favicons?domain=" + location.href;

		h2.appendChild(favicon);
		h2.appendChild(document.createTextNode(document.title));

		var input = document.createElement('input');
		input.style = "	background-image: url(\"http://www.google.com/s2/favicons?domain=" + location.href + "\");";
		input.id = "adesk_url";
		input.setAttribute("type", "text");
		var adesk_url_clear = location.href.replace("https://", "");
		adesk_url_clear = adesk_url_clear.replace("http://", "");
		adesk_url_clear = adesk_url_clear.replace("www.", "");
		input.setAttribute("value", adesk_url_clear);
		input.addEventListener("keydown", function(e)
		{
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13)
			{
				input.className = "eaeaea";

				var adesk_url_clear = this.value.replace("www.", "");
				adesk_url_clear = adesk_url_clear.replace("https://", "");
				adesk_url_clear = adesk_url_clear.replace("http://", "");
				adesk_url_clear = "http://" + adesk_url_clear;

 				window.location = adesk_url_clear;
			}
		});


		var menu = document.createElement('div');
		menu.id = "adesk_menu";

		var ul = document.createElement('ul');

			/* -------- li -------- */

			var li = document.createElement('li');
			li.className = "adesk_ul_li";

				var a = document.createElement('a');
				a.href = "http://example.com";
				a.className = "adesk_ul_li_a";
				a.id = "adesk_menu_open";
				a.appendChild(document.createTextNode('F1 | Escuchar menú'));
				a.addEventListener("click", function(event)
				{
					adesk_menu_open_function(event);
				});

			li.appendChild(a);

		ul.appendChild(li);

			/* -------- Fin li -------- */

			/* -------- li -------- */

			var li = document.createElement('li');
			li.className = "adesk_ul_li";

				var a = document.createElement('a');
				a.href = "http://example.com";
				a.className = "adesk_ul_li_a";
				a.id = "adesk_menu_open";
				a.appendChild(document.createTextNode('F2 | Abrir página'));
				a.addEventListener("click", function(event)
				{
					adesk_menu_open_function(event);
				});

			li.appendChild(a);

		ul.appendChild(li);

			/* -------- Fin li -------- */

			/* -------- li -------- */

			var li = document.createElement('li');
			li.className = "adesk_ul_li";

				var a = document.createElement('a');
				a.href = "http://example.com";
				a.className = "adesk_ul_li_a";
				a.id = "adesk_menu_back";
				a.appendChild(document.createTextNode('F3 | Atrás'));
				a.addEventListener("click", function(event)
				{
					adesk_menu_back_function(event);
				});

			li.appendChild(a);

		ul.appendChild(li);

			/* -------- Fin li -------- */

			/* -------- li -------- */

			var li = document.createElement('li');
			li.className = "adesk_ul_li";

				var a = document.createElement('a');
				a.href = "http://example.com";
				a.className = "adesk_ul_li_a";
				a.id = "adesk_menu_next";
				a.appendChild(document.createTextNode('F4 | Adelante'));
				a.addEventListener("click", function(event)
				{
					adesk_menu_next_function(event);
				});

			li.appendChild(a);

		ul.appendChild(li);

			/* -------- Fin li -------- */

			/* -------- li -------- */

			var li = document.createElement('li');
			li.className = "adesk_ul_li";

				var a = document.createElement('a');
				a.href = "http://example.com";
				a.className = "adesk_ul_li_a";
				a.id = "adesk_menu_next";
				a.appendChild(document.createTextNode('F5 | Más zoom'));
				a.addEventListener("click", function(event)
				{
					adesk_menu_zoom_plus_function(event);
				});

			li.appendChild(a);

		ul.appendChild(li);

			/* -------- Fin li -------- */

			/* -------- li -------- */

			var li = document.createElement('li');
			li.className = "adesk_ul_li";

				var a = document.createElement('a');
				a.href = "http://example.com";
				a.className = "adesk_ul_li_a";
				a.id = "adesk_menu_next";
				a.appendChild(document.createTextNode('F6 | Menos zoom'));
				a.addEventListener("click", function(event)
				{
					adesk_menu_zoom_minus_function(event);
				});

			li.appendChild(a);

		ul.appendChild(li);

			/* -------- Fin li -------- */
		menu.appendChild(input);
		menu.appendChild(h2);
		menu.appendChild(ul);

		//menu.setAttribute("style","background: red; position: absolute; top:0px; left: 0px; z-index: 9999999999999999999999");
		//document.body.setAttribute("style","position: absolute;  left: 250px;");
		document.body.appendChild(menu);

		var var_timer_loading = setInterval(function(){timer_loading()}, 1000);

		function timer_loading()
		{
			if(document.readyState == "complete")
			{
				input.className = "sss";
			}
			else
			{
				input.className = "eaeaea";
			}
		}

	}

	this.speak =function(text)
	{
		var imagen = document.createElement('img');	 
		imagen.setAttribute('src','http://localhost:61988/audio/' + text);
	}

	this.zoom_plus =function(text)
	{
		this.zoom = this.zoom + 0.1;
	}

	this.zoom_minus =function(text)
	{
		this.zoom = this.zoom - 0.1;
	}
}

var menu_browser = new menu_browser();

if(!document.getElementById("adesk_menu"))
{
	menu_browser.generate();
}

function adesk_menu_open_function(event)
{
	menu_browser.zoom_plus();

	var styles = document.createElement('style');
	styles.type = 'text/css';
	styles.innerHTML = 'body > *:not(#adesk_menu){zoom: ' + menu_browser.zoom + ';}';
	document.getElementsByTagName("head")[0].appendChild(styles);

	event.preventDefault();
	//menu_browser.speak("Atrás");
}

function adesk_menu_back_function(event)
{
	event.preventDefault()
	menu_browser.speak("Atras");
	history.back();
}

function adesk_menu_next_function(event)
{
	event.preventDefault()
	menu_browser.speak("Adelante");
	history.forward();
}

function adesk_menu_zoom_plus_function(event)
{
	menu_browser.zoom_plus();

	var styles = document.createElement('style');
	styles.type = 'text/css';
	styles.innerHTML = 'body > *:not(#adesk_menu){zoom: ' + menu_browser.zoom + ';}';
	document.getElementsByTagName("head")[0].appendChild(styles);

	event.preventDefault();
	//menu_browser.speak("Atrás");
}

function adesk_menu_zoom_minus_function(event)
{
	menu_browser.zoom_minus();

	var styles = document.createElement('style');
	styles.type = 'text/css';
	styles.innerHTML = 'body > *:not(#adesk_menu){zoom: ' + menu_browser.zoom + ';}';
	document.getElementsByTagName("head")[0].appendChild(styles);

	event.preventDefault();
	//menu_browser.speak("Atrás");
}


						//var h1 = document.createTextNode('Google');