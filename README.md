# README #

Adesk es una plataforma de aplicaciones para persionas en situación de discapacidad visual en estado alpha.

Puedes ver el video de demostración en: http://youtu.be/1lLWzXuoTeU


### Requerimientos ###

* Ubuntu (Linux)
* Instalar Festival: sudo apt-get install festival
* Versión en español de Festival: descargar desde http://forja.guadalinex.org/frs/download.php/154/festvox-sflpc16k_1.0-1_all.deb e instalar
* Webkit para Python: sudo apt-get install python-webkit && sudo apt-get install python-webkit-dev


### Cómo ejecutarlo ###

Ir a la carpeta del proyecto por consola y ejecutar ./start.py