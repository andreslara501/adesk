#!/usr/bin/env python
# -*- coding: utf-8 -*-

#==================================#
#              Adesk               #
#==================================#
#                                  #
# Application: Adesk-appcenter     #
# Internal Name: appcenter         #
# Versión: 0.2 - 2/10/2014         #
#                                  #
#==================================#

import os
import sys
import BaseHTTPServer
from urlparse import urlparse
import threading
import urllib 
from urllib import unquote

# Port to transmission of Adesk-Files server
PORT = 61988

# Path to start to check
PATH = "usr/bin"

#thread
threads = list()

class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
	print '\033[95m' + '** Adesk-Appcenter: Ok **' + '\033[0m'

	def do_GET (self):
		if self.path[1:] == 'stop':
			#If the request are "stop", stop server
			self.stop()

		else:
			path_utf8 = unquote_u(self.path)

			array_url = path_utf8.split('/')

			if array_url[1] == "audio":
				self.audio(0, 0, array_url[2])

			if array_url[1] == "epa":
				self.audio(0, 0, path_utf8)

			#By content ----------


			self.send_response(200)

			path_file_open = ""

			#Get variables
			if path_utf8.find("?") >= 0:
				path_utf8_tmp = path_utf8.split('?')
				path_utf8 = path_utf8_tmp[0]

			if array_url[1] == "system":
				self.send_header('Content-type','text/html')
				self.send_header('Access-Control-Allow-Origin', '*')                
				self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
				self.send_header("Access-Control-Allow-Headers", "X-Requested-With")		

			#Html type
			if path_utf8.endswith(".html"):
				self.send_header('Content-type','text/html')
				self.send_header('Access-Control-Allow-Origin', '*')                
				self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
				self.send_header("Access-Control-Allow-Headers", "X-Requested-With")

				path_file_open = PATH + "/" + path_utf8

			#Css type
			if path_utf8.endswith(".css"):
				self.send_header('Content-type','text/css')
				path_file_open = PATH + "/" + path_utf8

			#Javascript type
			if path_utf8.endswith(".js"):
				self.send_header('Content-type','application/javascript')
				path_file_open = PATH + "/" + path_utf8

			#Jpg type
			if path_utf8.endswith(".jpg"):
				self.send_header('Content-type','image/jpg')
				path_file_open = PATH + "/" + path_utf8

			#Png type
			if path_utf8.endswith(".png"):
				self.send_header('Content-type','image/png')
				path_file_open = PATH + "/" + path_utf8

			#Gif type
			if path_utf8.endswith(".gif"):
				self.send_header('Content-type','image/gif')
				path_file_open = PATH + "/" + path_utf8

			#Svg
			if path_utf8.endswith(".svg"):
				self.send_header('Content-type','image/svg+xml')
				path_file_open = PATH + "/" + path_utf8

			#Cssa type (Internal css app)
			if path_utf8.endswith(".cssa"):
				self.send_header('Content-type','text/css')
				path_file_open = "usr/share/themes/black" + "/" + path_utf8

			#Json type
			if path_utf8.endswith(".json"):
				self.send_header('Content-type','application/json')
				path_file_open = PATH + "/" + path_utf8

			#OTF font
			if path_utf8.endswith(".otf"):
				self.send_header('Content-type','font/opentype')
				path_file_open = PATH + "/" + path_utf8

			#PDF
			if path_utf8.endswith(".pdf"):
				self.send_header('Content-type','application/pdf')
				path_file_open = PATH + "/" + path_utf8

			self.end_headers()

			#Adding libraries and styles
			if path_utf8.endswith(".html"):
				styles = """
							<link rel="stylesheet" type="text/css" href="../styles/reset.css">
							<link rel="stylesheet" type="text/css" href="../app.cssa">
							<script src="../libs/js/jquery.js"></script async>
	        				<script src="../libs/js/hotkeys/jquery.hotkeys.js"></script async>
							<script src="../libs/core/core.js"></script async>
							<script src="../libs/core/file.js"></script async>
							<script src="../libs/core/configuration.js"></script async>
							<script src="../libs/core/menu.js"></script async>
							<script src="../libs/core/audio.js"></script async>
							<script src="../libs/core/controls.js"></script async>
							<script src="../libs/core/app.js"></script async>
							<script src="../libs/core/taskbar.js"></script async>
							"""
				self.wfile.write(styles)

			if array_url[1] == "system":
				ifconfig = os.popen('ifconfig')
				self.wfile.write(ifconfig.readlines())

			if len(path_file_open) > 0:
				file = open(path_file_open)
				self.wfile.write(file.read())
				file.close()

	def stop(self):
		#Stop server
		print '\033[91m' + '** Adesk-Appcenter: Server stopped, bye! **' + '\033[0m'
		httpd.server_close()
		httpd.stopped = True
		httpd.shutdown
		os._exit(0)


	def audio(aa, ee, ii, text):
		#Audio in console
		t = threading.Thread(target=audio_festival, args=(text,))
		threads.append(t)
		t.start()


def audio_festival(text):
	os.system('echo "' + text + '" | iconv -f utf-8 -t iso-8859-1 | festival --tts')

def unquote_u(source):
	result = unquote(source)

	if '%u' in result:
		result = result.replace('%u','\\u').decode('unicode_escape')

	return result

httpd = BaseHTTPServer.HTTPServer(('localhost', PORT), RequestHandler)
httpd.serve_forever()
